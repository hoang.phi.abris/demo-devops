#!/bin/sh

IMAGE=$1
docker build -t $REPOSITORY/$IMAGE:$TAG $IMAGE
docker push $REPOSITORY/$IMAGE:$TAG