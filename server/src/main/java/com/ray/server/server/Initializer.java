
package com.ray.server.server;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.ray.server.server.repositories.UserRepository;
import com.ray.server.server.models.User;

@Component
class Initializer implements CommandLineRunner {

    private UserRepository repository;

    public Initializer(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) {

    	ArrayList<User> users = new ArrayList<User>(); 

        User test1 = new User();
        test1.setName("Jack");

        User test2 = new User();
        test2.setName("Adam");
        
        User test3 = new User();
        test3.setName("Ray");
        test3.setEmail("ray@gmail.com");

    	users.add(test1);
        users.add(test2);
        users.add(test3);
        
    	System.out.println(users);
    	
        // Initial via code?!
        repository.saveAll(users);

        repository.findAll().forEach(System.out::println);
    }
}