package com.ray.server.server.controllers;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.CrossOrigin;


import com.ray.server.server.repositories.UserRepository;
import com.ray.server.server.models.User;
import java.util.Collection;
@RestController
public class UserController {
	@Autowired
	private UserRepository userRepository;

  @PostMapping(path="/add") // Map ONLY POST Requests
  public @ResponseBody String addNewUser (@RequestBody User user) {
    // @ResponseBody means the returned String is the response, not a view name
    // @RequestParam means it is a parameter from the GET or POST request

    User n = new User();
    n.setName(user.getName());
    n.setEmail(user.getEmail());
    userRepository.save(n);
    return "Saved";
  }

    
    @GetMapping("/")
	@CrossOrigin(origins = "http://client:3000")
    public Iterable<User> users() {
        return userRepository.findAll();
    }

	@GetMapping("/hello")
	public String home(){
		return "Hello from Ray";
	}
    
}