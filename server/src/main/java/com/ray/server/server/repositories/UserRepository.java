package com.ray.server.server.repositories;

import org.springframework.data.repository.CrudRepository;

import com.ray.server.server.models.User;

public interface UserRepository extends CrudRepository<User, Integer> {
}