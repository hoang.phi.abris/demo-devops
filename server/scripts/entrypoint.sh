#!/bin/sh
# Wait for the database to be up
if [ -n "db" ]; then
    ./wait-for-it.sh "db:3306"
fi
# Run the CMD
exec "$@"