CREATE TABLE user(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    email VARCHAR(100),
);

INSERT INTO user VALUES ("Adam","adam@gmail.com");
INSERT INTO user VALUES ("Ray","ray@gmail.com");
INSERT INTO user VALUES ("Hoang","hoang@gmail.com");
INSERT INTO user VALUES ("Michael","michael@gmail.com");
