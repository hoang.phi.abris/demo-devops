import React, { Component } from 'react';
import './App.css';
import {Container, Table } from 'reactstrap';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {users: [], isLoading: true};
  }

  componentDidMount() {
    this.setState({isLoading: true});
    fetch('/api')
      .then(response => response.json())
      .then(data => this.setState({users: data, isLoading: false}));
  }

  render() {
    const {users, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    const userlist = users.map(user => {
      return <tr key={user.id}>
        <td>{user.id}</td>
        <td style={{whiteSpace: 'nowrap'}}>{user.name}</td>
        <td>{user.email}</td>
      </tr>
    });

    return (
      <div>
        <Container fluid>
          <h3>User List</h3>
          <Table className="mt-4">
            <thead>
              <tr>
                <th width="20%">Id</th>
                <th width="20%">Name</th>
                <th width="20%">Email</th>
              </tr>
            </thead>
            <tbody>
            {userlist}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default App;