/**
 * Globals
 */

import React from 'react';
import './App.css';     


export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {users: [], isLoading: true};
  }
  
  render() {
    
    return (
      <div className="app" >
        <section className="container">
          <h1>Hello!</h1>
          <h2>Welcome to the internet.</h2>
          <p>You can find portainer from <a href="/portainer/">here!</a></p>
          </section>
      </div>
    );
  }
}
